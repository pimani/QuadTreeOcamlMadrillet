(* ------Library Load------ *)
# load "graphics.cma";;
# load "unix.cma";;
open Graphics;;
open Unix;;

(* ------Go to directory------ *)
#cd "/home/pimani/workspace/projet/QuadTreeOcamlMadrillet";;
(* #cd "/home/l2info/blondma7/BPF/mestpsdecamlblondma7/";; *)
(* #cd "/home/lightning/Bureau/projetbpf/";; *)

(* ------Creating Data Types------ *)

type coord = { x : int; y : int };;
type carre = { origin : coord; dim : int };;

type cquadtree =
  | Feuille of color
  | Racine of quadnode and quadnode = {
	sud_ouest : cquadtree;
	sud_est : cquadtree;
	nord_est : cquadtree;
  nord_ouest : cquadtree;
}

type quadtree = { carbre : cquadtree; arbredim : carre };;

(* ------Exception List------ *)
(* Default exceptions : https://caml.inria.fr/pub/docs/manual-ocaml/core.html *)
exception Invalid_file;;
exception File_not_found;;
exception Illegal_argument;;
exception Illegal_dimension;; (* à ajouter dans les fonctions avec test puissance de deux.*)

(* -------------------------------Show Zone---------------------------------------*)

(* Affiche une feuille dans la fenétre standard *)
let affiche_feuille feuille taille = set_color feuille;
	fill_rect taille.origin.x taille.origin.y taille.dim taille.dim;;

(* Affiche un arbre dans la fenêtre standard *)
let affiche_qt arbre = 
	let rec affiche_aux cellule taille = match cellule with
	| Feuille(cellule) -> affiche_feuille cellule taille
	| Racine(cellule) ->	affiche_aux cellule.sud_ouest		{ origin = { x = taille.origin.x; y = taille.origin.y }; dim = taille.dim / 2};
												affiche_aux cellule.sud_est   	{ origin = { x = taille.origin.x + taille.dim / 2; y = taille.origin.y }; 
																												dim = taille.dim / 2};
												affiche_aux cellule.nord_est  	{ origin = { x = taille.origin.x + taille.dim / 2; y = taille.origin.y + 
																												taille.dim / 2}; dim = taille.dim / 2};
												affiche_aux cellule.nord_ouest	{ origin = { x = taille.origin.x; y = taille.origin.y + taille.dim / 2}; 
																												dim = taille.dim / 2};
	in affiche_aux arbre.carbre arbre.arbredim;;

(* Convertit une chaîne en liste de caractères *)
let explode str =
  let rec expl i l =
    if i < 0 then 
			l 
		else 
			expl (i - 1) (str.[i] :: l) 
		in expl (String.length str - 1) [];;

(* Convertit une liste de caractères en une chaîne *)
let rec implode chars = 
	let rec impaux chars acc = 
		match chars with
  	| [] -> acc
    | h::t -> impaux t (acc^Char.escaped h)
		in impaux chars "";;

(* Renvoie le segment de la chaine sous forme de quadtree correspondant à la branche voulue *)
let divisestr chaine posvoulu =
	let rec divaux chaineaux poscurrent compteur res = match chaineaux with
 	| 'b'::reste -> if compteur - 1 = 0 && (poscurrent+1) = posvoulu 
										then res^"b"
        					else if compteur - 1 = 0 && (poscurrent+1) < posvoulu 
										then divaux reste (poscurrent+1) compteur ""
                	else divaux reste poscurrent (compteur-1) (res^"b")
 	| 'n'::reste -> if compteur - 1 = 0 && (poscurrent+1) = posvoulu 
										then res^"n"
        					else if compteur - 1 = 0 && (poscurrent+1) < posvoulu 
										then divaux reste (poscurrent+1) compteur ""
                	else divaux reste poscurrent (compteur-1) (res^"n")
	| '+'::reste -> divaux reste poscurrent (compteur-1+4) (res^"+")
	| texte -> raise (Invalid_argument (implode texte))
	in divaux chaine 0 1 "";;

(* Convertit une chaîne de caractères compatible en quadtree *)
let rec str_to_arb chaine = match explode chaine with
 	| 'b'::reste -> Feuille (white)                                                                      
 	| 'n'::reste -> Feuille (black)
	| '+'::reste -> Racine { sud_ouest = str_to_arb (divisestr reste 1); sud_est = str_to_arb (divisestr reste 2); 
													 nord_est = str_to_arb (divisestr reste 3); nord_ouest = str_to_arb  (divisestr reste 4)};
	| texte -> raise (Invalid_argument (implode texte));;

(*Creer un quadtree depuis un fichier QT*)
let charge_quadtree fichier = 
	let f = open_in fichier in
		let ext = input_line f in
		if ext = "QT" then begin
			let dimension = int_of_string (input_line f) 
  		and corps = input_line f in
				{carbre = (str_to_arb corps); arbredim = {origin = {x = 0; y = 0}; dim = dimension}}
				end
		else begin
			close_in f;
			raise Invalid_file end;;

(* Affiche une image depuis un fichier QT *)
let affiche_image fichier =
	let f = open_in fichier in
		let ext = input_line f in
		if ext = "QT" then begin
			let dimension = int_of_string (input_line f) 
  		and corps = input_line f in
    			let arbre = {carbre = (str_to_arb corps); arbredim = {origin = {x = 0; y = 0}; dim = dimension}} in
    				affiche_qt arbre;
					close_in f end
		else begin
			close_in f;
			raise Invalid_file end;;

(* ---------------------------------------------Save zone-----------------------------------------------*)

(* Ecrit dans un flux de données "n" si la feuille est noir, "b" sinon *)
let savefeuille couleur fichier =
	if couleur = black then
		output_string fichier "n"
	else
		output_string fichier "b";;

(* Ecrit dans un flux de donnés "+" si la cellule est une racine et appelle la fonction à ses branches sinon appelle savefeuille *)
let rec savearbre cellule f = match cellule with
	| Feuille(couleur) -> (savefeuille couleur f)
	| Racine(cel) ->			output_string f "+"; 
												savearbre cel.sud_ouest f;
												savearbre cel.sud_est f;
												savearbre cel.nord_est f;
												savearbre cel.nord_ouest f;;

(* Enregistre un quadtree dans un fichier créé avec le nom donné, si un fichier existe déjà avec le nom, il le supprimme *)
let sauve_quadtree nomfichier arbre =
	let f = open_out nomfichier in
	output_string f "QT\n";
	output_string f (string_of_int arbre.arbredim.dim);
	output_string f "\n";
	savearbre arbre.carbre f;
	close_out f;;

(* Change l'extension d'une chaîne de caractères *)
let changeext fileqt extension= 
	let rec changeaux fileqt res = match explode fileqt with
	| [] -> res^extension
  | '.'::t ->  changeaux "" (res^".")
	| x::t -> changeaux (implode t) (res^(implode [x]))
	in changeaux fileqt "";;

(*Renvoie la fin de la chaine après la sous-chaîne de longueur distance *)
let reste_chaine chaine distance =
	String.sub chaine distance ((String.length chaine) - distance);;

(*Crée une chaine de caractères à partir de 4 chaines en prenant taille caractère(s) à la suite de chaque chaine*)
let ligne l1 l2 taille =
	let rec ligneaux l1 l2 taille acc =
		if (String.length l1) <= taille then
			acc^l1^l2
		else
			ligneaux (reste_chaine l1 taille) (reste_chaine l2 taille) taille (acc^(String.sub l1 0 taille)^(String.sub l2 0 taille))
			in ligneaux l1 l2 taille "";;

(* Renvoie '1' si la feuille est noir, '0' sinon *)
let feuille_to_matrice feuille =
	if feuille = black then
		'1'
	else
		'0';;
                                                                                     
(* Convertit un arbre en matrice *)
let rec qt_to_matrice arbre taille = match arbre with
	| Feuille(cellule) -> String.make (taille*taille) (feuille_to_matrice cellule)
	| Racine(cellule) -> (ligne (qt_to_matrice cellule.nord_ouest (taille / 2)) (qt_to_matrice cellule.nord_est (taille / 2)) (taille / 2))^
	 	 			(ligne (qt_to_matrice cellule.sud_ouest (taille / 2)) (qt_to_matrice cellule.sud_est (taille / 2)) (taille / 2));;

(* Ecrit dans un fichier a partir d'un flux avec une taille maximum par ligne *)
let ecris_taille flux texte taille =
	let rec ecrisaux flux texte taille acc =
  	if (String.length texte) > taille then
  		ecrisaux flux (reste_chaine texte taille) taille (acc^(String.sub texte 0 taille)^"\n")
  	else
  		output_string flux (acc^texte)
		in ecrisaux flux texte taille "";;

(* Prend un fichier QT et renvoie son homologue PBM *)
let qt_en_pbm fileqt =
	let src = open_in fileqt and dest = open_out (changeext fileqt "pbm") in
		if (input_line src) = "QT" then begin
			output_string dest "P1\n";
			let taille = input_line src in
				output_string dest (taille^" "^taille^"\n");
				(ecris_taille dest (qt_to_matrice (str_to_arb (input_line src)) (int_of_string taille)) 70);
			close_in src;
			close_out dest end
		else begin
			close_in src;
			close_out dest;
			raise Invalid_file end;;

(* Teste si un nombre est une puissance de 2 *)
let puissance_de_deux x = (x <> 0) && ((land) x (x - 1) = 0);;

(* Renvoie la moitié gauche arrondie à l'inférieure d'une liste *)
let prendgauche liste = 
	let rec coupeg liste idx acc = 
		if idx < ((List.length liste) / 2) then
			coupeg liste (idx+1) (acc@[List.nth liste idx])
		else acc
		in coupeg liste 0 [];;

(* Renvoie la moitié droite arrondie au supérieur d'une liste *)
let prenddroite liste = 
	let rec couped liste idx acc = 
		if idx < (List.length liste) then
			couped liste (idx+1) (acc@[List.nth liste idx])	
		else acc
		in couped liste ((List.length liste) / 2) [];;

(* Renvoie la moitié gauche d'une chaine de caractère *)
let garde_gauche str =
	String.sub str 0 ((String.length str) / 2);;

(* Renvoie la moitié droite d'une chaine de caractère *)
let garde_droite str =
	String.sub str ((String.length str) / 2) ((String.length str) / 2);;

(* Equivalent recursif terminal de List.map *)
let map f l =
  let rec map_aux acc = function
    | [] -> acc []
    | x :: xs -> map_aux (fun ys -> acc (f x :: ys)) xs
  in
  map_aux (fun ys -> ys) l;;

(* Divise une liste représentant une matrice en une sous-matrice coorespondant à une branche d'un quadtree *)
let diviseliste liste indice =
	if indice = 1 then
		map garde_gauche (prenddroite liste)
	else if indice = 2 then
		map garde_droite (prenddroite liste)
	else if indice = 3 then
		map garde_droite (prendgauche liste)
	else if indice = 4 then
		map garde_gauche (prendgauche liste)
	else raise (Invalid_argument (string_of_int indice)) ;;

(* Convertit une liste de lignes d'une matrice pbm en quadtree *)
let rec listetoarbre liste = match liste with
	| ["0"] -> 	Feuille(white)
	| ["1"] -> 	Feuille(black)
	| 	_   -> 	if (diviseliste liste 1) = ["0"] && (diviseliste liste 2) = ["0"]
									&& (diviseliste liste 3) = ["0"] && (diviseliste liste 4) = ["0"] then
								Feuille(white)
							else if (diviseliste liste 1) = ["1"] && (diviseliste liste 2) = ["1"]
									&& (diviseliste liste 3) = ["1"] && (diviseliste liste 4) = ["1"] then
								Feuille(black)
							else
								Racine { sud_ouest = listetoarbre (diviseliste liste 1); sud_est = listetoarbre (diviseliste liste 2); 
											nord_est = listetoarbre (diviseliste liste 3); nord_ouest = listetoarbre (diviseliste liste 4)};;

(* Récupère une ligne de la longueur de sa matrice en gérant les retours à la ligne du format pbm *)
let repartir length file =
	let rec repaux length file acc =
  	let txt = input_line file in
  	if (String.length txt) >= length then
  		acc^txt
  	else if (String.length txt) > length - (String.length txt) then
  		acc^txt^(really_input_string file (length - (String.length txt)))
  	else if (String.length txt) < length then
  		repaux (length-(String.length txt)) file (acc^txt)
  	else acc^txt
		in repaux length file "";;

(* Enregistre chaque ligne de la matrice en tête d'une liste *)
let input_lines file length =
	let rec inputaux file length acc = 
		match try [repartir length file] with End_of_file -> [] with
  	|  [] -> acc
    | [line] -> inputaux file length (acc@[line])
    | _ -> raise Illegal_argument
		in inputaux file length [];;

(* Convertit un fichier pbm en fichier qt *)
let pbm_en_qt filepbm =
	let src = open_in filepbm and dest = open_out (changeext filepbm "QT") in
		if (input_line src) = "P1" then begin
			output_string dest "QT\n";
			let len = garde_gauche (input_line src) in
			output_string dest (len^"\n");
			savearbre (listetoarbre (input_lines src (int_of_string len))) dest;
			close_in src;
		  close_out dest end
		else begin
  		close_in src;
  		close_out dest;
  		raise File_not_found end;;

(* Renvoie la différence de taille entre deux fichiers, file1 et file2, en octets, *)
(* sinon renvoie l'erreur "fichier non disponible" si un fichier n'est pas accessible. *)
let compare_file file1 file2 = 
	if not(Sys.file_exists file1) || not(Sys.file_exists file2) then
		raise File_not_found
	else begin
		let f1 = (open_in file1) and f2 = (open_in file2) in 
		let x = (in_channel_length f1) - (in_channel_length f2) in
		close_in f1;
		close_in f2;
		x end;;

(* ------------------------------------------Test zone-----------------------------------------------*)

let arbcoord = { x = 0; y = 0};;
let arbdim = { origin = arbcoord; dim = 512};;


let c1arbre14n2 = Racine { sud_ouest = Feuille(white); sud_est = Feuille(black); nord_est = Feuille(white); nord_ouest = Feuille(white) };;

let c1arbre2n1 = Racine { sud_ouest = Feuille(white); sud_est = Feuille(white); nord_est = Feuille(black); nord_ouest = Feuille(black) };;
let c1arbre4n1 = Racine { sud_ouest = Feuille(black); sud_est = c1arbre14n2; nord_est = Feuille(black); nord_ouest = Feuille(white) };;

let c1arbren0 = Racine { sud_ouest = Feuille(black); sud_est = c1arbre2n1; nord_est = Feuille(white); nord_ouest = c1arbre4n1 };;

let figure1 = { carbre = c1arbren0; arbredim = arbdim };;


let c3arbre02n2 = Racine { sud_ouest = Feuille(white); sud_est = Feuille(black); nord_est = Feuille(black); nord_ouest = Feuille(white) };;
let c3arbre05n2 = Racine { sud_ouest = Feuille(black); sud_est = Feuille(white); nord_est = Feuille(white); nord_ouest = Feuille(black) };;
let c3arbre09n2 = Racine { sud_ouest = Feuille(white); sud_est = Feuille(black); nord_est = Feuille(black); nord_ouest = Feuille(white) };;
let c3arbre10n2 = Racine { sud_ouest = Feuille(black); sud_est = Feuille(white); nord_est = Feuille(white); nord_ouest = Feuille(black) };;
let c3arbre13n2 = Racine { sud_ouest = Feuille(white); sud_est = Feuille(black); nord_est = Feuille(black); nord_ouest = Feuille(white) };;
let c3arbre14n2 = Racine { sud_ouest = Feuille(black); sud_est = Feuille(white); nord_est = Feuille(white); nord_ouest = Feuille(black) };;

let c3arbre1n1 = Racine { sud_ouest = Feuille(white); sud_est = c3arbre02n2; nord_est = Feuille(black); nord_ouest = Feuille(white) };;
let c3arbre2n1 = Racine { sud_ouest = c3arbre05n2; sud_est = Feuille(white); nord_est = Feuille(white); nord_ouest = Feuille(black) };;
let c3arbre3n1 = Racine { sud_ouest = c3arbre09n2; sud_est = c3arbre10n2; nord_est = Feuille(black); nord_ouest = Feuille(white) };;
let c3arbre4n1 = Racine { sud_ouest = c3arbre13n2; sud_est = c3arbre14n2; nord_est = Feuille(white); nord_ouest = Feuille(black) };;

let c3arbren0 = Racine { sud_ouest = c3arbre1n1; sud_est = c3arbre2n1; nord_est = c3arbre3n1; nord_ouest = c3arbre4n1 };;

let figure3 = { carbre = c3arbren0; arbredim = arbdim };;

sauve_quadtree "image_v.QT" figure1;;
sauve_quadtree "image_v3.QT" figure3;;
qt_en_pbm "image_v3.QT";;
pbm_en_qt "chromo.pbm";;
pbm_en_qt "lena.pbm";;

qt_en_pbm "lena.QT";;

let dimimage = " "^(string_of_int figure1.arbredim.dim)^"x"^(string_of_int figure1.arbredim.dim);;

open_graph dimimage;;

affiche_qt figure1;;
Unix.sleep 3;;
affiche_qt figure3;;
Unix.sleep 3;;
affiche_image "image_v.QT";;
Unix.sleep 3;;
affiche_image "image_v3.QT";;
affiche_image "chromo.QT";;
affiche_image "lena.QT";;

close_graph;;


